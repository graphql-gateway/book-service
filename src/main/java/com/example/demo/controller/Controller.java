package com.example.demo.controller;

import com.example.demo.dto.Book;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

  @GetMapping("/books")
  public List<Book> getBooks() {
    Book book1 = Book.builder().author("Author 1").numberOfPages(167).build();
    Book book2 = Book.builder().author("Author 2").numberOfPages(789).build();
    return Arrays.asList(book1, book2);
  }

}
